# Run from the phusion base image
FROM ubuntu

# Change to /home to download Simple-Help
WORKDIR /home

RUN  apt-get update && \
     apt-get install -y wget sed rsync

# Download the most up to date SimpleHelp version
RUN wget https://simple-help.com/releases/SimpleHelp-linux-amd64.tar.gz

# Extract the archive, then remove it and 
RUN tar -zxvf SimpleHelp-linux-amd64.tar.gz  && \
    rm SimpleHelp-linux-amd64.tar.gz && \
    rm -Rf /home/SimpleHelp/configuration

# remove the & sign so that the server doesn't background
RUN sed -i 's/&//g' ./SimpleHelp/serverstart.sh

# Clean up packages
RUN apt-get install -y libc6-i386

COPY startup-script.sh /home

RUN apt-get purge && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

# RUN ln -s /home/startup-script.sh / # backwards compatability
CMD ["sh","startup-script.sh"]
